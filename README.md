# SOBRE

É um plugin QGIS que a partir de uma imagem de satélite sentinel 2 identifica telhados e gera um raster e um vetor.

# ABOUT

This is a QGIS plugin that uses an open QGIS project satellite image from bing provided by QuickMapService on projection EPSG:3857. Image pieces are saved temporally into the machine to be used later by recognition onnx algorithm, to identify roofs and save them inside a postgresql + postgis table. 



# INSTALAÇÃO

No terminal python do QGIS instalar:

import pip
pip.main(['install','earthengine-api'])
pip.main(['install', 'requests'])

Depois abra a janela complementos, pesquise por RoofDraw e instale o plugin

## Desenvolvimento

Criar uma chave de acesso .json do serviço gee e adicionar a pasta root, atualizar o nome do arquivo no arquivo roof_draw.py

# INSTALATION


Open terminal python from QGIS and install:

import pip
pip.main(['install','earthengine-api'])
pip.main(['install', 'requests'])

After you need to open QGIS plugin instalation window, search and install RoofDraw plugin.

## Development

Create access key .json to gee service add to the root folder, update key call inside file roof_draw.py

# DOCUMENTAÇÃO

Para mais informações consulte a documentação em: https://gitlab.com/roofdraw/roof_draw/-/wikis/ROOFDRAW

# DOCUMENTATION

For more information vizualize the documentation: https://gitlab.com/roofdraw/roof_draw/-/wikis/ROOFDRAW
