from qgis.core import QgsCoordinateReferenceSystem
from osgeo import osr
import roof_draw.templates as templates
from qgis.core import QgsProviderRegistry
from qgis.utils import iface
import ee
from .water import water
from .agriculture import agriculture
from .urban import urban
from .forest import forest


DLG_GEE_CLASSIFIER = None

first_start_gee_classifier = None

iface = iface

# PARA CLASSIFICACAO SUPPERVISIONADA
# labels = urban.merge(water).merge(forest).merge(agriculture)

labels = urban.merge(forest).merge(agriculture)
