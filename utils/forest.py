import ee

forest = ee.FeatureCollection(
  [ee.Feature(
      ee.Geometry.Point([-55.38928660097043, -24.02668231650657]),
      {
        "system:index": "0","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.44696482362668, -24.07684465919056]),
      {
        "system:index": "1","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.41400583925168, -24.202164628431454]),
      {
        "system:index": "2","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.38654001893918, -24.084367319458888]),
      {
        "system:index": "3","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.47992380800168, -24.126987385807663]),
      {
        "system:index": "4","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.46893747987668, -24.2171947632233]),
      {
        "system:index": "5","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.36731394472043, -24.2547623407755]),
      {
        "system:index": "6","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.45795115175168, -24.269786265861956]),
      {
        "system:index": "7","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.24921091737668, -24.179616103353087]),
      {
        "system:index": "8","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-56.16107615175168, -23.644810691412786]),
      {
        "system:index": "9","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.86993845643918, -23.941361402258238]),
      {
        "system:index": "10","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.74908884706418, -23.911234652971423]),
      {
        "system:index": "11","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-55.72436960878293, -24.19214355388263]),
      {
        "system:index": "12","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-57.07568796815793, -24.17711046563961]),
      {
        "system:index": "13","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.822792584891175, -25.2294825208837]),
      {
        "system:index": "14","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.685463483328675, -25.274196726619504]),
      {
        "system:index": "15","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.745888288016175, -25.289097802976432]),
      {
        "system:index": "16","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.833778913016175, -25.467767697420573]),
      {
        "system:index": "17","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.927162702078675, -25.51239382126325]),
      {
        "system:index": "18","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.78366138623488, -25.513712372666838]),
      {
        "system:index": "19","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.74658252881301, -25.532301493957807]),
      {
        "system:index": "20","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.72048999951613, -25.381026550357394]),
      {
        "system:index": "21","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.80151416943801, -25.340075283454347]),
      {
        "system:index": "22","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.82623340771926, -25.269308628390654]),
      {
        "system:index": "23","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.03497364209426, -25.52982311084284]),
      {
        "system:index": "24","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.22723438428176, -25.543453584234136]),
      {
        "system:index": "25","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.33435108350051, -25.56327696112854]),
      {
        "system:index": "26","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.41537525342238, -25.599198470017885]),
      {
        "system:index": "27","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.43048145459426, -25.628918458492944]),
      {
        "system:index": "28","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.47168018506301, -25.64501369877076]),
      {
        "system:index": "29","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.12698414014113, -25.511233605611853]),
      {
        "system:index": "30","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-54.29589893506301, -25.55088773484277]),
      {
        "system:index": "31","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.82623340771926, -25.376063499433727]),
      {
        "system:index": "32","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.86331226514113, -25.34131643473202]),
      {
        "system:index": "33","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.80014087842238, -25.400876714241733]),
      {
        "system:index": "34","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.75344898389113, -25.210926118051106]),
      {
        "system:index": "35","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.68890430615676, -25.199743290347442]),
      {
        "system:index": "36","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.69851734326613, -25.204713562822032]),
      {
        "system:index": "37","cobertura":4
      }),
  ee.Feature(
      ee.Geometry.Point([-53.76168872998488, -25.155001711338567]),
      {
        "system:index": "38","cobertura":4
      })])
