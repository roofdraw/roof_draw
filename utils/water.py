import ee

water = ee.FeatureCollection(
        [
        ee.Feature(
            ee.Geometry.Point([-54.59869775853458, -25.0793967869117]),
            {
              "system:index": "1","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.5726052292377, -25.074110442482404]),
            {
              "system:index": "2","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.54239282689395, -25.077842003418848]),
            {
              "system:index": "3","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.51492700658145, -25.08499384399227]),
            {
              "system:index": "4","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.49192438206973, -25.099296271445283]),
            {
              "system:index": "5","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.47578821263614, -25.084682903090645]),
            {
              "system:index": "6","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.449695683339264, -25.10395974497818]),
            {
              "system:index": "7","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.437679386952546, -25.121368474741036]),
            {
              "system:index": "8","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.430469609120514, -25.14250431263873]),
            {
              "system:index": "9","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.43733606419864, -25.185386381628238]),
            {
              "system:index": "10","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.460682011464264, -25.205268598155357]),
            {
              "system:index": "11","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.457403762378334, -25.03152234260614]),
            {
              "system:index": "12","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.432684524097084, -24.974270976115378]),
            {
              "system:index": "13","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.41208515886271, -24.87837753209994]),
            {
              "system:index": "14","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.39835224870646, -24.812329663569322]),
            {
              "system:index": "15","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.347540481128334, -24.730032146221713]),
            {
              "system:index": "16","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.314581496753334, -24.572767530207496]),
            {
              "system:index": "17","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.314581496753334, -24.623962673541914]),
            {
              "system:index": "18","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.490362746753334, -24.88211503610205]),
            {
              "system:index": "19","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.366766555347084, -24.749988067476234]),
            {
              "system:index": "20","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.43405781511271, -24.903292089236498]),
            {
              "system:index": "21","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.46652690641602, -25.21828612145237]),
            {
              "system:index": "22","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.489872853681646, -25.275423282982857]),
            {
              "system:index": "23","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.49673930875977, -25.32508591413104]),
            {
              "system:index": "24","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.544804494306646, -25.353632708435146]),
            {
              "system:index": "25","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.56540385954102, -25.39085753073568]),
            {
              "system:index": "26","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.445927541181646, -25.041737027712966]),
            {
              "system:index": "27","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.42807475797852, -24.923481015448004]),
            {
              "system:index": "28","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.451420705244146, -25.047957876096717]),
            {
              "system:index": "29","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.418461720869146, -25.036760121792323]),
            {
              "system:index": "30","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.42795106009528, -25.22635417946171]),
            {
              "system:index": "31","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.395678721228094, -25.239397858482093]),
            {
              "system:index": "32","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.38057252005622, -25.231944498967337]),
            {
              "system:index": "33","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.38881226614997, -25.261755194429906]),
            {
              "system:index": "34","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.326400721724404, -24.114200721298193]),
            {
              "system:index": "35","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.34700008695878, -24.148039340446807]),
            {
              "system:index": "36","cobertura":0
            }),
        ee.Feature(
            ee.Geometry.Point([-54.33223720854081, -24.18938548737261]),
            {
              "system:index": "37","cobertura":0
            })])
            
