import ee

agriculture = ee.FeatureCollection(
  [ee.Feature(
      ee.Geometry.Point([-54.17260991424207, -25.212868012978102]),
      {
        "system:index": "0","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.17576419204358, -25.210557792900154]),
      {
        "system:index": "1","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.18207274764661, -25.212130300408578]),
      {
        "system:index": "2","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.18548451751355, -25.21135375603034]),
      {
        "system:index": "3","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.18548451751355, -25.2159935350393]),
      {
        "system:index": "4","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.17009936660413, -25.214634622268704]),
      {
        "system:index": "5","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.16831837981824, -25.21785716215901]),
      {
        "system:index": "6","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.16162358611707, -25.215022884608953]),
      {
        "system:index": "7","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.15546523421887, -25.211916751198533]),
      {
        "system:index": "8","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.208704153702776, -25.210461015643727]),
      {
        "system:index": "9","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.35197691288779, -25.254549514715553]),
      {
        "system:index": "10","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.32279447880576, -25.2449233728775]),
      {
        "system:index": "11","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.30356840458701, -25.235917582022598]),
      {
        "system:index": "12","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.38218931523154, -25.20423674234115]),
      {
        "system:index": "13","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.599855941208105, -25.226911123933142]),
      {
        "system:index": "14","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.602602523239355, -25.26479585969633]),
      {
        "system:index": "15","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.21997066801347, -24.162449366088154]),
      {
        "system:index": "16","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.14546963041581, -24.1759181373209]),
      {
        "system:index": "17","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.275932276900186, -24.19470939642809]),
      {
        "system:index": "18","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.443473780806436, -24.19001184114928]),
      {
        "system:index": "19","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-54.04487606352128, -24.174352074138707]),
      {
        "system:index": "20","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.95320888822831, -24.174352074138707]),
      {
        "system:index": "21","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.97586818998612, -24.118587713948816]),
      {
        "system:index": "22","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.68437577873434, -24.59094499012586]),
      {
        "system:index": "23","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.66892625480856, -24.575959260157806]),
      {
        "system:index": "24","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.64249040275778, -24.543171726951442]),
      {
        "system:index": "25","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.472545639574186, -24.597812850342017]),
      {
        "system:index": "26","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.41933061271872, -24.569090201409658]),
      {
        "system:index": "27","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.417613998949186, -24.575022592502386]),
      {
        "system:index": "28","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.96568234481127, -24.964317262769054]),
      {
        "system:index": "29","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.93855984725268, -24.956224590672264]),
      {
        "system:index": "30","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.94405301131518, -24.964006015988392]),
      {
        "system:index": "31","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.946112947838614, -24.990770361392602]),
      {
        "system:index": "32","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.84792264022143, -24.962449770278212]),
      {
        "system:index": "33","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.80088742293627, -24.962449770278212]),
      {
        "system:index": "34","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.71025021590502, -24.913885024307827]),
      {
        "system:index": "35","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.65085537947924, -24.917932817411515]),
      {
        "system:index": "36","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.62579281844408, -24.927584710993166]),
      {
        "system:index": "37","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.63437588729174, -24.90360926068129]),
      {
        "system:index": "38","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.74114926375658, -24.893955491153868]),
      {
        "system:index": "39","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.808783846276114, -24.89644685879145]),
      {
        "system:index": "40","cobertura":1
      }),
  ee.Feature(
      ee.Geometry.Point([-53.78543789901049, -25.012550992786366]),
      {
        "system:index": "41","cobertura":1
      })])

