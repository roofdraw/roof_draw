import ee

urban = ee.FeatureCollection(
        [ee.Feature(
            ee.Geometry.Point([-53.745682884022486, -24.713922742159546]),
            {
              "system:index": "0","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74579553680111, -24.713576753510175]),
            {
              "system:index": "1","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.7449104078262, -24.71311380944709]),
            {
              "system:index": "2","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74476020412136, -24.714000711299875]),
            {
              "system:index": "3","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.743478108212244, -24.713128428759617]),
            {
              "system:index": "4","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74352638797451, -24.713235636999016]),
            {
              "system:index": "5","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.741707850262415, -24.71318203289086]),
            {
              "system:index": "6","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.738950539395105, -24.71414690330638]),
            {
              "system:index": "7","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74165420608212, -24.714955829305858]),
            {
              "system:index": "8","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.7410748489349, -24.71523846486168]),
            {
              "system:index": "9","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.7433439977615, -24.715296941103464]),
            {
              "system:index": "10","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.7433439977615, -24.714965575370183]),
            {
              "system:index": "11","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.744958687588465, -24.715175115568773]),
            {
              "system:index": "12","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74486212806393, -24.71467806615163]),
            {
              "system:index": "13","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.742684174343836, -24.713654722867204]),
            {
              "system:index": "14","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.741241145893824, -24.713191779094053]),
            {
              "system:index": "15","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.738489199444544, -24.71360111893949]),
            {
              "system:index": "16","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74724392966915, -24.714595224389154]),
            {
              "system:index": "17","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.747211743160975, -24.714721923532977]),
            {
              "system:index": "18","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.747217107579004, -24.715019178710367]),
            {
              "system:index": "19","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.745951104923975, -24.714668320064792]),
            {
              "system:index": "20","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.746466089054834, -24.71424923760878]),
            {
              "system:index": "21","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74805395679165, -24.713518276460345]),
            {
              "system:index": "22","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74765162543942, -24.714239491488378]),
            {
              "system:index": "23","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74761407451321, -24.715233591840306]),
            {
              "system:index": "24","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.74441688136746, -24.71415664943405]),
            {
              "system:index": "25","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75303176951451, -24.71269137839995]),
            {
              "system:index": "26","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75466955076467, -24.714485960211633]),
            {
              "system:index": "27","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.7541357149443, -24.714500579363047]),
            {
              "system:index": "28","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75226889746993, -24.71406200407449]),
            {
              "system:index": "29","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.753711925919944, -24.713901192748214]),
            {
              "system:index": "30","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75362609523147, -24.713482107709744]),
            {
              "system:index": "31","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.753427611764366, -24.71394017733124]),
            {
              "system:index": "32","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75357245105117, -24.714334895546735]),
            {
              "system:index": "33","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.75506912368148, -24.713652665744487]),
            {
              "system:index": "34","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.807679166093386, -24.713766209009357]),
            {
              "system:index": "35","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.80862893529585, -24.70959991597846]),
            {
              "system:index": "36","cobertura":3
            }),
        ee.Feature(
            ee.Geometry.Point([-53.80905540652922, -24.7094610285041]),
            {
              "system:index": "37","cobertura":3
            })])