# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=RoofDraw
qgisMinimumVersion=3.0
description=This is a QGIS plugin that uses Google Earth Engine sentinel 2 image collection in projection EPSG:4326. the images classified using random forest algoritm and the result tiff is downloaded converted in vector file type shape and open inside current QGIS project.
version=1.0.0
author=PTI-TERRITORIO-ITDT-OPENSOURCE
email=nitterritorio@gmail.com

about=Develop by: Eixo território do Núcleo de Inteligência Territorial - NIT, Inteligência e Gestão Territorial - IT.DT e Fundação Parque Tecnológico Itaipu – Brasil em parceria com a Itaipu. Instructions of how to install required Python libs you can find in: https://gitlab.com/roofdraw/roof_draw/-/blob/main/README.md

tracker=https://gitlab.com/roofdraw/roof_draw/-/issues
repository=https://gitlab.com/roofdraw/roof_draw.git
homepage=https://gitlab.com/roofdraw/roof_draw/-/wikis/ROOFDRAW

icon=icon.png


