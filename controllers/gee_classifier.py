import ee
import requests
import roof_draw.utils as utils
import datetime
from qgis.gui import QgsMapToolEmitPoint
import roof_draw.templates as templates
from qgis.core import QgsCoordinateReferenceSystem
from qgis.gui import QgsExtentGroupBox
from qgis.utils import iface
import os
from qgis import processing
from qgis.core import QgsProject, QgsRasterLayer, QgsVectorLayer, QgsField
from PyQt5.QtCore import QVariant

extent_group_box = QgsExtentGroupBox()

def carregar_extent():
    canvas = iface.mapCanvas()
    canvas_extent = canvas.extent()
    print(
        canvas_extent.xMinimum(),
        canvas_extent.xMaximum(),
        canvas_extent.yMinimum(),
        canvas_extent.yMaximum()
    )

    extent_group_box.setOriginalExtent(
        canvas_extent,
        canvas.mapSettings().destinationCrs()
    )
    extent_group_box.setCurrentExtent(
        canvas_extent,
        QgsCoordinateReferenceSystem('EPSG:4326')
    )
    extent_group_box.setOutputCrs(QgsCoordinateReferenceSystem('EPSG:4326'))

    extent_group_box.show()


def initialize_gee_classifier():
    utils.DLG_GEE_CLASSIFIER.btn_coordenates.clicked.connect(carregar_extent)

def run_gee_classifier():
    """Run method that performs all the real work"""
    # Create the dialog with elements (after translation) and keep reference
    # Only create GUI ONCE in callback, so that it will only load when the plugin is started
    if utils.first_start_gee_classifier == True:
        utils.first_start_gee_classifier = False
        utils.DLG_GEE_CLASSIFIER = templates.RoofDrawGeeClassifierDialog()
        initialize_gee_classifier()
    # show the dialog
    utils.DLG_GEE_CLASSIFIER.show()
    # Run the dialog event loop
    result = utils.DLG_GEE_CLASSIFIER.exec_()
    utils.iface.messageBar().pushMessage("INFO", "Processo Iniciado Por Favor Aguarde !...", level=0)  
    if result:
        ok_gee_classify()
def maskS2clouds(image):
    qa = image.select('QA60')
    # Bits 10 e 11 são nuvens
    cloudBitMask = 1 << 10
    cirrusBitMask = 1 << 11
    # 0 igual tempo limpo
    mask = qa.bitwiseAnd(cloudBitMask).eq(0).And(qa.bitwiseAnd(cirrusBitMask).eq(0))
    return image.updateMask(mask).divide(10000)

def generate_image(gee_date, gee_date_next):
    """
    Criar imagem classificada de telhados usando sentinel2
    """
    area_coordinates = generate_area_coordinates()
    area = ee.Geometry.Polygon(coords=area_coordinates, proj=None, geodesic=False)   
    dataset = ee.ImageCollection('COPERNICUS/S2_HARMONIZED').filterDate(gee_date, gee_date_next).filterBounds(area).filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE',5)).map(maskS2clouds)
    return dataset.select('B.*')
    

def generate_area_coordinates():
    # não achei na documentacao do qgis a unidade se é km2, m2 ou outra... 
    if(extent_group_box.currentExtent().area()<=1):
        extents_split = extent_group_box.currentExtent().asPolygon().split(",")
        extents_coord1 = extents_split[0].split()
        extents_coord2 = extents_split[2].split()
        extents_coord3 = extents_split[3].split()
        extents_coord4 = extents_split[4].split()
        area_coordinates = [[[float(extents_coord1[0]), float(extents_coord1[1])],
                [float(extents_coord2[0]), float(extents_coord2[1])],
                [float(extents_coord3[0]), float(extents_coord3[1])],
                [float(extents_coord4[0]), float(extents_coord4[1])]]]
        return area_coordinates
    else:
        utils.iface.messageBar().pushMessage("ERROR", "Área escolhida passou de 1, Verifique tamanho da área e se o QGIS esta em ESPG:4326", level=2)

def download(classified, bands, nome_area, gee_date, gee_date_next):
    """
    Baixar o TIFF
    """
    area_coordinates = generate_area_coordinates()
    area = ee.Geometry.Polygon(coords=area_coordinates, proj=None, geodesic=False)       
    url_download = classified.getDownloadURL({'bands': bands, 'region': area, 'scale': 20,'format': 'GEO_TIFF'})
    response = requests.get(url_download)
    if(response.status_code == 200):
        print(response)
        open("/tmp/"+nome_area+".tiff", "wb").write(response.content)
        """
        Gerar Vetor a partir do TIFF
        """
        # utils.iface.addRasterLayer("/tmp/"+nome_area+".tiff", nome_area)
        current_raster = QgsRasterLayer("/tmp/"+nome_area+".tiff", nome_area)
        QgsProject.instance().addMapLayer(current_raster)
        current_vector_output = processing.run("gdal:polygonize", {'FIELD':'classify','INPUT': current_raster, 'OUTPUT': "/tmp/"+nome_area+".shp", 'BAND':1})
        current_vector = QgsVectorLayer(current_vector_output['OUTPUT'], nome_area)
        current_vector.dataProvider().addAttributes([QgsField('description', QVariant.String)])
        current_vector.updateFields()
        QgsProject.instance().addMapLayer(current_vector)
    else:
        utils.iface.messageBar().pushMessage("ERROR", "Nenhuma imagems válida encontrada para o período de "+gee_date+" até "+gee_date_next, level=2)

def ok_gee_classify():      
    """
    Dados de entrada
    """
    day = utils.DLG_GEE_CLASSIFIER.dateEdit.date().day()
    month = utils.DLG_GEE_CLASSIFIER.dateEdit.date().month()
    year = utils.DLG_GEE_CLASSIFIER.dateEdit.date().year()
    day_next = utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().day()
    month_next = utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().month()
    year_next = utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().year()
    gee_date = str(year)+"-"+str(month)+"-"+str(day)
    gee_date_next = str(year_next)+"-"+str(month_next)+"-"+str(day_next)
    nome_area = utils.DLG_GEE_CLASSIFIER.name_area.text()+"_"+gee_date+"_"+gee_date_next
    classification_type = 'randomforest'
    if(classification_type=='supervised'):
        if(utils.DLG_GEE_CLASSIFIER.dateEdit.date().toPyDate()<utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().toPyDate()):
            dataset = generate_image(gee_date, gee_date_next)            
            img_mean = dataset.mean()
            if(img_mean.bandNames().getInfo()!=[]):
                ndwi = img_mean.normalizedDifference(['B3', 'B8']).rename('NDWI')
                img_mean = img_mean.addBands(ndwi)
                ndvi = img_mean.normalizedDifference(['B5', 'B4']).rename('NDVI')
                img_mean = img_mean.addBands(ndvi)
                # NDBI=(TM5−TM4)/(TM5+TM4)  
                # fonte calculo: https://www.researchgate.net/publication/248977308_Use_of_normalized_difference_built-up_index_in_automatically_mapping_urban_areas_from_TM_imagery
                ndbi = img_mean.normalizedDifference(['B11', 'B8']).rename('NDBI')
                img_mean = img_mean.addBands(ndbi)
                ndbivi = img_mean.normalizedDifference(['NDVI', 'NDBI']).rename('NDBIVI')
                img_mean = img_mean.addBands(ndbivi)
                bands = ['B11', 'B8', 'B3', 'B5','B4']
                #posiciona os pontos com label para obter o treinamento
                training = img_mean.select(bands).sampleRegions(collection=utils.labels,properties= ['cobertura'],scale= 15)
                if(training.size().getInfo()>0):
                    # treina
                    trained = ee.Classifier.smileCart().train(training, 'cobertura', bands)
                    # classifica
                    classified = img_mean.select(bands).classify(trained)
                    download(classified, ['classification'], nome_area, gee_date, gee_date_next)
                else:
                    utils.iface.messageBar().pushMessage("ERROR", "As imagens encontradas não possibilitaram criar um treinamento válido escolha uma período diferente de: "+gee_date+" até "+gee_date_next, level=2)
            else:
                utils.iface.messageBar().pushMessage("ERROR", "Não foi possível baixar imagem classificada para os parâmetros informados!", level=2)   
        else:
            utils.iface.messageBar().pushMessage("ERROR", "Data inicial deve ser menor que Data final!", level=2)
    elif classification_type=='unsupervised':
        if(utils.DLG_GEE_CLASSIFIER.dateEdit.date().toPyDate()<utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().toPyDate()):
            # PARA CLASSIFICACAO NÂO SUPERVISIONADA
            region = ee.Geometry.Polygon(
                    [[[-54.51628781921104, -25.33943739195488],
                    [-54.51628781921104, -25.44117000925637],
                    [-54.21965695983604, -25.44117000925637],
                    [-54.21965695983604, -25.33943739195488]]], None, False)
            dataset = generate_image(gee_date, gee_date_next)
            img_mean = dataset.mean()
            # construindo dataset de treinamento
            training = img_mean.sample(region= region,scale= 30,numPixels=5000)

            # clusteriza e treina
            clusterer = ee.Clusterer.wekaKMeans(15).train(training)
            result = img_mean.cluster(clusterer)
            download(result, ['cluster'], nome_area, gee_date, gee_date_next)
        else:
            utils.iface.messageBar().pushMessage("ERROR", "Data inicial deve ser menor que Data final!", level=2)
    elif classification_type=='randomforest':
        if(utils.DLG_GEE_CLASSIFIER.dateEdit.date().toPyDate()<utils.DLG_GEE_CLASSIFIER.dateEdit_2.date().toPyDate()):
            area_coordinates = generate_area_coordinates()
            area = ee.Geometry.Polygon(coords=area_coordinates, proj=None, geodesic=False)   
            dataset = generate_image(gee_date, gee_date_next)
            img_mosaic = dataset.mosaic()
            # ESA WorldCover land cover map, fonte para labels e classificacao.
            landcover = ee.Image('ESA/WorldCover/v100/2020')
            # Remap valores da classe cobertura para uma serie sequecial.
            class_values = [10, 20, 30, 40, 50, 60, 70, 80, 90, 95, 100]
            remap_values = ee.List.sequence(0, 10)
            label = 'landcover'
            landcover = landcover.remap(class_values, remap_values).rename(label).toByte()
            # Adiciona o cobertura como uma banda e gera uma amostra 100 pixels aos 10m de escala de cada ponto de cobertura na regiao de interesse
            sample = img_mosaic.addBands(landcover).stratifiedSample(numPoints= 100,classBand= label,region= area,scale= 10,geometries= True)
            # Adiciona valores randomicos a amostra e usa para separar 80% das features para o dataset de treinamento e 20% para validacao
            sample = sample.randomColumn()          
            training_sample = sample.filter('random <= 0.9')
            validation_sample = sample.filter('random > 0.9')
            # Criar uma arvore de treinamento de 40-tree para o classificador random forest classifier.
            trained_classifier = ee.Classifier.smileRandomForest(40).train(features= training_sample, classProperty= label, inputProperties= img_mosaic.bandNames())
            # Classifica
            result = img_mosaic.classify(trained_classifier)
            download(result, ['classification'], nome_area, gee_date, gee_date_next)
            # Informacoes sobre o classificador
            validation_sample = validation_sample.classify(trained_classifier)
            validation_accuracy = validation_sample.errorMatrix(label, 'classification')
            utils.iface.messageBar().pushMessage("INFO", 'Resultados do classificador treinado:' + str(trained_classifier.explain().getInfo()), level=0)
            train_accuracy = trained_classifier.confusionMatrix()
            utils.iface.messageBar().pushMessage("INFO", 'Acuracia treinamento:' + str(train_accuracy.accuracy().getInfo()), level=0)
            validation_sample = validation_sample.classify(trained_classifier)
            validation_accuracy = validation_sample.errorMatrix(label, 'classification')
            utils.iface.messageBar().pushMessage("INFO", 'Acuracia validacao:' +str(validation_accuracy.accuracy().getInfo()), level=0)
        else:
            utils.iface.messageBar().pushMessage("ERROR", "Data inicial deve ser menor que Data final!", level=2)