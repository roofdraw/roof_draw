import ee
import os

this_path = os.path.dirname(os.path.abspath(__file__))
print(this_path)
"""
Autenticar como servico no GEE - adicionar arquivo chave qgis-plugin-354112-44aa7849a89c.json na pasta
"""
service_account = 'gee-qgis@qgis-plugin-354112.iam.gserviceaccount.com'
credentials = ee.ServiceAccountCredentials(service_account, this_path+'/qgis-plugin-354112-44aa7849a89c.json')
ee.Initialize(credentials)